#!/usr/bin/env bash

sudo docker swarm init
sudo docker network create -d overlay docker-airflow
sudo docker stack deploy -c docker-stack.yml airflow
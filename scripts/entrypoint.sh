#!/bin/bash

TRY_LOOP="20"

: "${AIRFLOW__CORE__FERNET_KEY:=${FERNET_KEY:=$(python -c "from cryptography.fernet import Fernet; FERNET_KEY = Fernet.generate_key().decode(); print(FERNET_KEY)")}}"
: "${AIRFLOW__CORE__EXECUTOR:="LocalExecutor"}"

: "${AIRFLOW__CORE__DAGS_ARE_PAUSED_AT_CREATION:="true"}"
: "${AIRFLOW__CORE__LOAD_EXAMPLES:="true"}"
: "${AIRFLOW__API__AUTH_BACKEND:="airflow.api.auth.backend.basic_auth"}"


export \
  AIRFLOW__CORE__FERNET_KEY \
  AIRFLOW__CORE__EXECUTOR


wait_for_port() {
  local name="$1" host="$2" port="$3"
  local j=0
  while ! nc -z "$host" "$port"; do
    j=$((j+1))
    if [[ $j -ge $TRY_LOOP ]]; then
      echo >&2 "$(date) - $host:$port still not reachable, giving up"
      exit 1
    fi
    echo "$(date) - waiting for $name... $j/$TRY_LOOP"
    sleep 5
  done
}


if [[ -z "$AIRFLOW__CORE__SQL_ALCHEMY_CONN" ]]; then
  : "${POSTGRES_HOST:="postgres"}"
  : "${POSTGRES_PORT:="5432"}"
  : "${POSTGRES_USER:="airflow"}"
  : "${POSTGRES_PASSWORD:="airflow"}"
  : "${POSTGRES_DB:="airflow"}"
  : "${POSTGRES_EXTRAS:-""}"

  AIRFLOW__CORE__SQL_ALCHEMY_CONN="postgresql+psycopg2://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}${POSTGRES_EXTRAS}"
  export AIRFLOW__CORE__SQL_ALCHEMY_CONN

  if [[ "$AIRFLOW__CORE__EXECUTOR" == "CeleryExecutor" ]]; then
    AIRFLOW__CELERY__RESULT_BACKEND="db+postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}${POSTGRES_EXTRAS}"
    export AIRFLOW__CELERY__RESULT_BACKEND
  fi

else
  if [[ "$AIRFLOW__CORE__EXECUTOR" == "CeleryExecutor" && -z "$AIRFLOW__CELERY__RESULT_BACKEND" ]]; then
    >&2 printf '%s\n' "FATAL: if you set AIRFLOW__CORE__SQL_ALCHEMY_CONN manually with CeleryExecutor you must also set AIRFLOW__CELERY__RESULT_BACKEND"
    exit 1
  fi

  POSTGRES_ENDPOINT=$(echo -n "$AIRFLOW__CORE__SQL_ALCHEMY_CONN" | cut -d '/' -f3 | sed -e 's,.*@,,')
  POSTGRES_HOST=$(echo -n "$POSTGRES_ENDPOINT" | cut -d ':' -f1)
  POSTGRES_PORT=$(echo -n "$POSTGRES_ENDPOINT" | cut -d ':' -f2)
fi

wait_for_port "Postgres" "$POSTGRES_HOST" "$POSTGRES_PORT"


if [[ "$AIRFLOW__CORE__EXECUTOR" == "CeleryExecutor" ]]; then
  if [[ -z "$AIRFLOW__CELERY__BROKER_URL" ]]; then
    : "${REDIS_PROTO:="redis://"}"
    : "${REDIS_HOST:="redis"}"
    : "${REDIS_PORT:="6379"}"
    : "${REDIS_PASSWORD:=""}"
    : "${REDIS_DBNUM:="0"}"

    # When Redis is secured by basic auth, it does not handle the username part of basic auth, only a token
    if [[ -n "$REDIS_PASSWORD" ]]; then
      REDIS_PREFIX=":${REDIS_PASSWORD}@"
    else
      REDIS_PREFIX=
    fi

    AIRFLOW__CELERY__BROKER_URL="${REDIS_PROTO}${REDIS_PREFIX}${REDIS_HOST}:${REDIS_PORT}/${REDIS_DBNUM}"
    export AIRFLOW__CELERY__BROKER_URL

  else
    # Derive useful variables from the AIRFLOW__ variables provided explicitly by the user
    REDIS_ENDPOINT=$(echo -n "$AIRFLOW__CELERY__BROKER_URL" | cut -d '/' -f3 | sed -e 's,.*@,,')
    REDIS_HOST=$(echo -n "$POSTGRES_ENDPOINT" | cut -d ':' -f1)
    REDIS_PORT=$(echo -n "$POSTGRES_ENDPOINT" | cut -d ':' -f2)
  fi

  wait_for_port "Redis" "$REDIS_HOST" "$REDIS_PORT"
fi


: "${AIRFLOW_ADMIN_EMAIL:="airflow@example.org"}"
: "${AIRFLOW_ADMIN_FIRSTNAME:="airflow"}"
: "${AIRFLOW_ADMIN_LASTNAME:="airflow"}"
: "${AIRFLOW_ADMIN_USERNAME:="airflow"}"
: "${AIRFLOW_ADMIN_PASSWORD:="airflow"}"

case "$@" in
  "webserver")
    airflow db init
    airflow users create -e "$AIRFLOW_ADMIN_EMAIL" \
                          -f "$AIRFLOW_ADMIN_FIRSTNAME" \
                          -l "$AIRFLOW_ADMIN_LASTNAME" \
                          -r Admin \
                          -u "$AIRFLOW_ADMIN_USERNAME" \
                          -p "$AIRFLOW_ADMIN_PASSWORD"
    exec airflow "$@"
    ;;
  "celery worker"|"scheduler")
    sleep 10
    exec airflow "$@"
    ;;
  "celery flower")
    sleep 20
    exec airflow "$@"
    ;;
  *)
    "$@"
esac
